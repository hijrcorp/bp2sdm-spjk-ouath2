$( document ).ready(function() {
	$.backstretch(ctx + "/images/backgrounds/1.jpg");
	$("form").submit(function(e){
		if($('[name=source]').length ){
			$('[name=username]').val($('[name=username]').val()+'::'+$('[name=source]').val());
		}
		if($('[name=password]').length ){
			if($('[name=password]').val() == ''){
				alert('Password tidak boleh kosong, harap diisi.');
				$('#btnLogin').val('Update');
				e.preventDefault();
				return false;
			}
		}
		if($('[name=email]').length ){
			if($('[name=email]').val() != '') {
				if(!validateEmail($('[name=email]').val())){
					alert('Format email tidak benar, harap diperbaiki.');
					$('#btnLogin').val('Update');
					e.preventDefault();
					return false;
				}
			}
		}
		$('#btnLogin').text("Sedang proses..");
	});
	
	$('[name=source]').on('change', function(){ $("[name=password]").focus(); });
	
	if($('[name=password]').length ){ $("[name=password]").focus(); }
	else{ $("[name=username]").focus(); }
	
	$("#show_password").on('click', function(event) {
		event.preventDefault();
		if($('#password').attr("type") == "text"){
			$('#password').attr('type', 'password');
			$('#show_password i').addClass( "fa-eye-slash" );
			$('#show_password i').removeClass( "fa-eye" );
		}else if($('#password').attr("type") == "password"){
			$('#password').attr('type', 'text');
			$('#show_password i').removeClass( "fa-eye-slash" );
			$('#show_password i').addClass( "fa-eye" );
		}
	});
	getSelectGolongan();
});


function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

function ajaxGET(url, fnsuccess, fnerror, param){
	$.ajax({
		url: url,
		method: "GET",
		success: function (response) {
			if (fnsuccess instanceof Function) {
				fnsuccess(response, param);
			} else {
				var fn = window[fnsuccess];
				if(typeof fn === 'function') {
					fn(response, param);
				}
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			if (fnerror instanceof Function) {
				fnerror(jqXHR);
			} else {
				var fn = window[fnerror];
				if(typeof fn === 'function') {
					fn(jqXHR);
				}
			}
		}
	});
}

function getSelectGolongan(){
	$('[name=id_golongan]').empty();
	$('[name=id_golongan]').append(new Option('Pilih Golongan', ''));
	//filter=kode_simpeg.not(null)
	ajaxGET(ctx + '/restql' + '/golongan?','onGetSelectGolongan','onGetSelectError');
}
function onGetSelectGolongan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_golongan]').append(new Option(value.nama, value.id));
	});
}
function onGetSelectError(response){
	console.log(response);
}
