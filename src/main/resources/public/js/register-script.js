$(document).ready(function() {
	
	$.backstretch(ctx + "/images/backgrounds/1.jpg");
	
	$("form").submit(function(e){
		if($('[name=source]').length ){
			$('[name=username]').val($('[name=username]').val()+'::'+$('[name=source]').val());
		}
		if($('[name=password]').length ){
			if($('[name=password]').val() == ''){
				alert('Password tidak boleh kosong, harap diisi.');
				$('#btnLogin').val('Update');
				e.preventDefault();
				return false;
			}
		}
		if($('#password').val() != $('#password2').val() ){
			alert('Konfirmasi Password tidak sama, harap dicek kembali.');
			$('#btnLogin').val('Update');
			e.preventDefault();
			return false;
		}
		if($('[name=email]').length ){
			if($('[name=email]').val() != '') {
				if(!validateEmail($('[name=email]').val())){
					alert('Format email tidak benar, harap diperbaiki.');
					$('#btnLogin').val('Update');
					e.preventDefault();
					return false;
				}
			}
			
		}
		$('#btnLogin').text("Sedang proses..");
		/*var list_tmp_disabled = [];
		$.each($('form').find('input:disabled'),function(){
			list_tmp_disabled.push(this);
			$(this).prop('disabled',false);
		});
		$.each(list_tmp_disabled,function(){
			$(this).prop('disabled',false);
		});*/
		e.preventDefault();
		
		hideAlert();
		
		var obj = new FormData(document.querySelector('form'));
		$.ajax({
			url: ctx+'/register',
			method: "POST",
			crossDomain: true,
			contentType: false,
			processData: false,
			data : obj,
			cache: false,
			success: function(response) {
				console.log('response: ', response);
				showAlert(response.data.status.message, 'success');
			},
			error: function(response) {
				console.log('response.responseJSON.', response.responseJSON);
				showAlert(response.responseJSON.message);
			},
			complete: function() {
				$('#btnLogin').text("Register");
			}
		});
		
		return false;
	});
	
	$('[name=source]').on('change', function(){
		$("[name=password]").focus();
	});
		
	if($('[name=password]').length ){
		$("[name=password]").focus();
	}else{
		$("[name=username]").focus();
	}
	$("#show_password").on('click', function(event) {
		event.preventDefault();
		if($('#password').attr("type") == "text"){
			$('#password').attr('type', 'password');
			$('#show_password i').addClass( "fa-eye-slash" );
			$('#show_password i').removeClass( "fa-eye" );
		}else if($('#password').attr("type") == "password"){
			$('#password').attr('type', 'text');
			$('#show_password i').removeClass( "fa-eye-slash" );
			$('#show_password i').addClass( "fa-eye" );
		}
	});
	$("#show_password2").on('click', function(event) {
		event.preventDefault();
		if($('#password2').attr("type") == "text"){
			$('#password2').attr('type', 'password');
			$('#show_password i').addClass( "fa-eye-slash" );
			$('#show_password i').removeClass( "fa-eye" );
		}else if($('#password2').attr("type") == "password"){
			$('#password2').attr('type', 'text');
			$('#show_password i').removeClass( "fa-eye-slash" );
			$('#show_password i').addClass( "fa-eye" );
		}
	});
});


function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

function disabledForm() {
	$('[name=full_name],[name=email],[name=mobile],[name=password],[name=password2]').val('');
	$('[name=f]').find('input:not([name=username])').prop('disabled',true);
	$('#btnLogin').prop('disabled',true);
	$('[name=f]').find('input[type=password]').parent().show();
}
/*
	private Status status1 = new Status(1, "Silahkan isi Form Registrasi.");
	private Status status2 = new Status(2, "NIP anda sudah terdaftar pada aplikasi ${appBefore}, silahkan mendaftar akun ${appDestination}.");
	private Status status3 = new Status(3, "NIP anda dikunci disemua aplikasi BP2SDM. Silahkan kontak Admin.");
	private Status status4 = new Status(6, "NIP anda tidak terdaftar pada aplikasi SIMPEG.");
	private Status status5 = new Status(4, "NIP anda sudah terdaftar pada aplikasi ${appDestination} sebagai ${siapGroup} dan sedang menunggu persetujuan ADMIN."); //SIAP BerAKHLAK
	private Status status6 = new Status(5, "NIP anda sudah terdaftar pada aplikasi ${appDestination} sebagai ${siapGroup}. Silahkan Login."); //SIAP BerAKHLAK*/

function btnCheckNip(){
	if($('[name=username]').val()=='') {
		showAlert('NIP masih kosong!');
		return;
	}
	btnCheckNipEnabled(false);
	$.ajax({
		url: ctx+'/check-account?username='+$('#username').val(),
		method: "GET",
		success: function (response) {
			btnCheckNipEnabled(true);
			console.log('response.data: ', response.data);
			disabledForm();
			if(response.data.status.id == 1) {
				$('[name=f]').find('button,input,select').prop('disabled',false);
				$('[name=full_name]').val(response.data.name).prop('disabled',true);
				$('[name=email]').val(response.data.email);
				$('[name=mobile]').val(response.data.mobile);
				$('[name=idEselon1]').val(response.data.eselon1.id);
				$('[name=idUnitAuditi]').val(response.data.unitAuditi.id_unit_auditi);
				$('[name=idJabatan]').val(response.data.jabatan.id);
				if(response.data.unitAuditi.id_unit_auditi != null) $('[name=idUnitAuditi]').prop('disabled',true);
			}
			else if(response.data.status.id == 2) {
				$('[name=f]').find('input:not([name=username])').prop('disabled',true);
				$('[name=full_name]').val(response.data.name);
				$('[name=email]').val(response.data.email);
				$('[name=mobile]').val(response.data.mobile);
				$('[name=idEselon1]').val(response.data.eselon1.id);
				$('[name=idUnitAuditi]').val(response.data.unitAuditi.id_unit_auditi);
				$('[name=idJabatan]').val(response.data.jabatan.id);
				if(response.data.unitAuditi.id_unit_auditi != null) $('[name=idUnitAuditi]').prop('disabled',true);
				$('[name=password],[name=password2]').val('-1');
				$('[name=f]').find('input[type=password]').parent().hide();
				$('[name=f]').find('button').prop('disabled',false);
			}
			else if(response.data.status.id == 3) { }
			else if(response.data.status.id == 4) { }
			else if(response.data.status.id == 5) { }
			else if(response.data.status.id == 6) {
				//location = './';
			}
			console.log(response.data.status.message);
			showAlert(response.data.status.message, (response.data.status.id == 1 || response.data.status.id == 2 || response.data.status.id == 5 || response.data.status.id == 6 ? 'info' : 'danger'));
		},
		error: function (response) {
			btnCheckNipEnabled(true);
			//alert('Connection Error.');
			console.log('error: ', response.responseJSON.message);
			showAlert(response.responseJSON.message);
		}
	});
}

function btnCheckNipEnabled(enable=true){
	$("#btnCheck").html(enable?'Check':'...').prop('disabled',enable!=true);
}

function showAlert(msg='', alert='danger'){
	$('#msg-alert').html('<div class="alert alert-'+alert+' alert-dismissible show" role="alert">'+msg+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
}

function hideAlert(){
	$('#msg-alert').html('');
}