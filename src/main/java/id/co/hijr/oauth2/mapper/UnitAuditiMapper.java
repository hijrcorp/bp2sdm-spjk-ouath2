package id.co.hijr.oauth2.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.UnitAuditi;

public interface UnitAuditiMapper {

	List<UnitAuditi> getList(QueryParameter param);

	UnitAuditi getEntity(String id);

	List<Map<String, Object>> getListByKodeSimpeg(String kode);
	Map<String, Object> getMappingById(String id);

	@Update("UPDATE spjk_unit_auditi SET id_account_deleted_unit_auditi=#{userId}, timestamp_deleted_unit_auditi=now() WHERE id_unit_auditi=#{idUnitAuditi}")
	void deleteTemp(@Param("idUnitAuditi") String idUnitAuditi, @Param("userId") String userId);
	


	//satker
	@Select("SELECT id_eselon1_unit_auditi FROM tbl_unit_auditi_user a LEFT JOIN spjk_unit_auditi b ON b.id_unit_auditi = a.unit_auditi_id WHERE user_id=#{userId}")
	List<String> findEselon1ByUnitAuditiUserId(@Param("userId") String userId);
	
	@Insert("INSERT INTO tbl_unit_auditi_user (unit_auditi_id, user_id) VALUES (#{unitAuditiId}, #{userId})")
	void insertUnitAuditiUser(@Param("unitAuditiId") String unitAuditiId, @Param("userId") String userId);

	@Delete("DELETE FROM tbl_unit_auditi_user WHERE unit_auditi_id=#{unitAuditiId} and user_id=#{userId}")
	void deleteUnitAuditiUser(@Param("unitAuditiId") String unitAuditiId, @Param("userId") String userId);

	@Delete("DELETE FROM tbl_unit_auditi_user WHERE user_id=#{userId}")
	void deleteAllUnitAuditiUser(@Param("userId") String userId);
	
}
