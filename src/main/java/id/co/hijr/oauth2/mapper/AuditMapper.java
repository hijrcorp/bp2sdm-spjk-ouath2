package id.co.hijr.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.oauth2.model.Audit;
import id.co.hijr.oauth2.model.QueryParameter;



@Mapper
public interface AuditMapper {
	
	@Insert("INSERT INTO hijr_audit (id_audit, date_audit, type_audit, code_audit, details_audit, id_source_audit, account_added_audit, timestamp_added_audit, account_modified_audit, timestamp_modified_audit) VALUES (#{id:VARCHAR}, #{date:DATE}, #{type:VARCHAR}, #{code:VARCHAR}, #{details:VARCHAR}, #{idSource:VARCHAR}, #{accountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{accountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(Audit audit);

	@Update("UPDATE hijr_audit SET id_audit=#{id:VARCHAR}, date_audit=#{date:DATE}, type_audit=#{type:VARCHAR}, code_audit=#{code:VARCHAR}, details_audit=#{details:VARCHAR}, id_source_audit=#{idSource:VARCHAR}, account_added_audit=#{accountAdded:VARCHAR}, timestamp_added_audit=#{timestampAdded:TIMESTAMP}, account_modified_audit=#{accountModified:VARCHAR}, timestamp_modified_audit=#{timestampModified:TIMESTAMP} WHERE id_audit=#{id}")
	void update(Audit audit);

	@Delete("DELETE FROM hijr_audit WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_audit WHERE id_audit=#{id}")
	void delete(Audit audit);

	List<Audit> getList(QueryParameter param);

	Audit getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
