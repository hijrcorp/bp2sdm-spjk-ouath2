package id.co.hijr.oauth2.mapper;

import id.co.hijr.oauth2.model.Sesi;

public interface SesiMapper {

	Sesi getEntityActive();

}
