package id.co.hijr.oauth2.mapper;

import java.util.List;

import id.co.hijr.oauth2.model.Eselon1;
import id.co.hijr.oauth2.model.QueryParameter;

public interface Eselon1Mapper {

	List<Eselon1> getList(QueryParameter param);

	Eselon1 getEntity(String id);
}
