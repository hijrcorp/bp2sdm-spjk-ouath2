package id.co.hijr.oauth2.mapper;

import java.util.List;

import id.co.hijr.oauth2.model.MasterJabatan;
import id.co.hijr.oauth2.model.QueryParameter;

public interface MasterJabatanMapper {

	List<MasterJabatan> getList(QueryParameter param);

}
