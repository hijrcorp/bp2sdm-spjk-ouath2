package id.co.hijr.oauth2.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import id.co.hijr.oauth2.model.AccountLogin;
//import id.co.hijr.oauth2.domain.OauthUserOrganization;

@Configuration
public class CustomTokenEnhancer implements TokenEnhancer {
	
	@Value("${user.picture.url}")
	private String pictureUrl;
	
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		final Map<String, Object> additionalInfo = new HashMap<>();

		//OauthUserOrganization user = (OauthUserOrganization) authentication.getPrincipal();
		AccountLogin user = (AccountLogin) authentication.getPrincipal();

		System.out.println("authentication: " + authentication);
		System.out.println("authentication.getPrincipal(): " + authentication.getPrincipal());
		System.out.println("authentication.getPrincipal().toString(): " + authentication.getPrincipal().toString());
		
//		additionalInfo.put("organization_id", user.getOrganizationId());
//		additionalInfo.put("organization_name", user.getOrganizationName());
		additionalInfo.put("source_id", user.getSource().getId());
		additionalInfo.put("source_name", user.getSource().getId());
		additionalInfo.put("account_id", user.getId());
		additionalInfo.put("full_name", user.getFullName());
		additionalInfo.put("account_avatar", pictureUrl+user.getId());
		additionalInfo.put("email", user.getEmail());
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
		return accessToken;
	}

}
