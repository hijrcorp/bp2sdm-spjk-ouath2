//package id.co.hijr.oauth2.services;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.MediaType;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import id.co.hijr.oauth2.Utils;
//import id.co.hijr.oauth2.mapper.AccountLoginMapper;
//import id.co.hijr.oauth2.mapper.LoginMapper;
//import id.co.hijr.oauth2.model.AccountLogin;
//import id.co.hijr.oauth2.model.Login;
//import id.co.hijr.oauth2.model.QueryParameter;
//import id.co.hijr.oauth2.model.Source;
//import id.co.hijr.oauth2.security.TwoFactorAuthenticationInterceptor;
//import id.co.hijr.oauth2.template.BearerAuthRestTemplate;
//
//@Service
//public class PerangkatService {
//	
//	private Logger logger = LoggerFactory.getLogger(getClass());
//	
//	@Value("${app.url.rest}")
//	private String urlRest = "";
//	
//	@Value("${ntfc.app.code}")
//	private String code = "";
//	
//	@Value("${ntfc.app.config}")
//	private String config = "";
//	
//	private final String COOKIE_NAME = "DEVICE_ID";
//	
//	private static final String SAVE_PATH = "/akun_/perangkat/save?id_generator=uuid";
//	private static final String LIST_PATH = "/akun_/perangkat/list";
//	
//	public String getDeviceId(HttpServletRequest request) {
//		Cookie[] cookies = request.getCookies();
//
//		String devid = "";
//		if(cookies != null) {
//			for (int i = 0; i < cookies.length; i++) {
//				String name = cookies[i].getName();
//				String value = cookies[i].getValue();
//				if (name.equals(COOKIE_NAME)) {
//					devid = value;
//				}
//			}
//		}
//
//		return devid;
//	}
//	
//	@Autowired
//	private LoginMapper loginMapper;
//	
//	
//	public void register(HttpServletRequest request, String accessToken) {
//		
//		String devid = getDeviceId(request);
//		
//		Map<String, String> map = getBrowserInfo(request);
//		
//		String tipe = map.get("browser").toString();
//		String nama = map.get("details").toString();
//		
//		QueryParameter param = new QueryParameter();
//		param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+accessToken+"'");
////		param.setClause(param.getClause() + " AND " + Login.EXPIRE_TIME + " > NOW()");
////		param.setClause(param.getClause() + " AND " + Login.MFA_CODE_EXPIRED + " > NOW()");
//		
//		List<Login> lstLogin = loginMapper.getList(param);
//		if(lstLogin.size() > 0 && !devid.equals("")) {
//			Login login = lstLogin.get(0);
//			
//			String urlGet = urlRest+"/me"+LIST_PATH+"?filter=kode.eq("+devid+")";
//			
//			Map<String,Object> ret= Utils.getRest(urlGet, new BearerAuthRestTemplate(accessToken));
//			String id = "";
//			List<Map<String,Object>> list =  (List<Map<String,Object>>) ret.get("data");
//			
//			if(list.size() == 0) {
//				Map<String, Object> payload = new HashMap<String, Object>();
//				
//				
//				Map<String, Object> data = new HashMap<>();
//				data.put("id_account", login.getAccountId());
//				data.put("nama", nama);
//				data.put("tipe", tipe);
//				data.put("kode", devid);
//				
//				payload.put("data", data);
//				
//				
//				String url = urlRest+SAVE_PATH;
//				
//				Map<String,Object> resp= Utils.postRestJson(payload, url, new BearerAuthRestTemplate(accessToken));
//			
//			}
//			
//			
//			
//		}
//		
//	}
//	
//	public void generate(HttpServletRequest request, HttpServletResponse response) {
//		
//		if(getDeviceId(request).equals("")) {
//			
//			Cookie sessionCookie = new Cookie( COOKIE_NAME, Utils.getUUIDString());
//			int exp = 24*3600*365*10;
//			sessionCookie.setMaxAge(exp);
//			response.addCookie( sessionCookie );
//			
//		}
//		
//		
//	}
//	
//	public Map<String, String> getBrowserInfo(HttpServletRequest request) {
//		Map<String, String> map = new HashMap<>();
//		
//		String  browserDetails  =   request.getHeader("User-Agent");
//		String  userAgent	   =   browserDetails;
//		String  user			=   userAgent.toLowerCase();
//
//		String os = "";
//		String browser = "";
//
//		logger.info("User Agent for the request is===>"+browserDetails);
//		//=================OS=======================
//		 if (userAgent.toLowerCase().indexOf("windows") >= 0 )
//		 {
//			 os = "Windows";
//		 } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
//		 {
//			 os = "Mac";
//		 } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
//		 {
//			 os = "Unix";
//		 } else if(userAgent.toLowerCase().indexOf("android") >= 0)
//		 {
//			 os = "Android";
//		 } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
//		 {
//			 os = "IPhone";
//		 }else{
//			 os = "UnKnown, More-Info: "+userAgent;
//		 }
//		 //===============Browser===========================
//		if (user.contains("msie"))
//		{
//			String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
//			browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
//		} else if (user.contains("safari") && user.contains("version"))
//		{
//			browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
//		} else if ( user.contains("opr") || user.contains("opera"))
//		{
//			if(user.contains("opera"))
//				browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
//			else if(user.contains("opr"))
//				browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
//		} else if (user.contains("chrome"))
//		{
//			browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
//		} else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
//		{
//			//browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
//			browser = "Netscape-?";
//
//		} else if (user.contains("firefox"))
//		{
//			browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
//		} else if(user.contains("rv"))
//		{
//			browser="IE-" + user.substring(user.indexOf("rv") + 3, user.indexOf(")"));
//		} else
//		{
//			browser = "UnKnown, More-Info: "+userAgent;
//		}
//		logger.info("Operating System======>"+os);
//		logger.info("Browser Name==========>"+browser);
//		
//		map.put("os", os);
//		map.put("browser", browser);
//		map.put("details", browserDetails);
//		
//		
//		return map;
//	}
//}
