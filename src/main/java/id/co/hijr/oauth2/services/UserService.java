package id.co.hijr.oauth2.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import id.co.hijr.oauth2.mapper.AccountLoginMapper;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.Source;

@Service
@Configuration
public class UserService implements UserDetailsService {

	@Value("${source.default.id}")
	private String sourceDefaultId;
	
	@Autowired
	private AccountLoginMapper accountLoginMapper;
	
	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
			String[] userSrc =  s.split("::");
			String userId = userSrc[0];
			String srcId = sourceDefaultId;
			if(userSrc.length > 1) {
				srcId = userSrc[1];
			}
			
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + Source.ID + "='"+srcId+"'");
			param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
			param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");
			// param.setClause(param.getClause() + " OR " + AccountLogin.EMAIL + "='"+userId+"'");
			// param.setClause(param.getClause() + " OR " + AccountLogin.MOBILE + "='"+userId+"'");
			param.setClause(param.getClause() + ")");
			List<AccountLogin> lst = accountLoginMapper.getList(param);
			
		if (lst.size() > 0 ) {
			AccountLogin accountLogin = lst.get(0);
			Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
//			HashMap<String, String> paramRole = new HashMap<>();
			List<String> roles = accountLoginMapper.getListRole(accountLogin.getId());
			for (String role : roles) {
				grantedAuthorities.add(new SimpleGrantedAuthority(role.trim()));
			}
			
//			System.out.println(accountLogin.isAdmin());
			if(accountLogin.isAdmin()) {
				grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			}
			
			accountLogin.setAuthorities(grantedAuthorities);
//			accountLogin.setUsername(userId+"::"+orgId);
			accountLogin.setUsername(accountLogin.getId()+"::"+accountLogin.getSource().getId()+"::"+accountLogin.getUsername());
			
			return accountLogin;
		} else {
			System.out.println("UsernameNotFoundException");
			throw new UsernameNotFoundException(String.format("User with login ID [%s] not found", s));
		}
	}

   
}
