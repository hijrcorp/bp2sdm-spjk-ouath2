package id.co.hijr.oauth2.services;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.mapper.AccountMapper;
import id.co.hijr.oauth2.mapper.LoginMapper;
import id.co.hijr.oauth2.model.Account;
import id.co.hijr.oauth2.model.Login;
import id.co.hijr.oauth2.model.QueryParameter;

public class HijrTokenService extends DefaultTokenServices {
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private LoginMapper loginMapper;
	
	@Override
	public OAuth2Authentication loadAuthentication(String accessTokenValue) throws AuthenticationException, InvalidTokenException {
		System.out.println("loadAuthentication");
		OAuth2Authentication auth = super.loadAuthentication(accessTokenValue);
		return auth;
	}

	@Override
	public OAuth2AccessToken readAccessToken(String accessToken) {
		System.out.println("readAccessToken");
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+accessToken+"'");
		List<Login> lstLogin = loginMapper.getList(param);
		if(lstLogin.size() > 0) {
			return super.readAccessToken(accessToken);
		}
		return null;
	}
	
	@Override
	public synchronized OAuth2AccessToken createAccessToken(OAuth2Authentication authentication) throws AuthenticationException {
		
		String clientId = authentication.getOAuth2Request().getClientId();
		String userName = authentication.getName();
		Account userDetails = (Account) authentication.getPrincipal();
		
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Login.CLIENT_ID + "='"+clientId+"'");
		param.setClause(param.getClause() + " AND " + Login.ACCOUNT_ID + "='"+userDetails.getId()+"'");
		
		System.out.println("HijrTokenService.createAccessToken : " + userDetails.getId());
		Login login = null;
		OAuth2AccessToken accessToken = null;
		loginMapper.deleteBatch(param);
		/*
		Login login = null;
		List<Login> lstLogin = loginMapper.getList(param);
		if(lstLogin.size() > 0) {
			login = lstLogin.get(0);
		}
		
		OAuth2AccessToken accessToken = null;
		
		if(login != null) {
			accessToken = (DefaultOAuth2AccessToken)SerializationUtils.deserialize(login.getTokenObject());
			
		}else {
		*/	
			accessToken = super.createAccessToken(authentication);
			
			login = new Login();
			login.setId(Utils.getUUIDString());
			login.setAccessToken(accessToken.getValue());
			login.setRefreshToken(accessToken.getRefreshToken().getValue());
			login.setClientId(authentication.getOAuth2Request().getClientId());
			login.setUsername(userName);
			login.setCreatedTime(new Date());
			login.setExpireTime(accessToken.getExpiration());
			login.setTokenObject(SerializationUtils.serialize((DefaultOAuth2AccessToken)accessToken));
			login.setSourceId(accessToken.getAdditionalInformation().get("source_id").toString());
			login.setAccountId(userDetails.getId());
			loginMapper.insert(login);
			
			accountMapper.updateLogin(new Account(accessToken.getAdditionalInformation().get("account_id").toString()));
		//}
		
		return accessToken;
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
	public OAuth2AccessToken refreshAccessToken(String refreshTokenValue, TokenRequest tokenRequest) throws AuthenticationException {
		OAuth2AccessToken accessToken = null;
		
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Login.REFRESH_TOKEN + "='"+refreshTokenValue+"'");
		
		List<Login> lstLogin = loginMapper.getList(param);
		if(lstLogin.size() > 0) {
			Login login = lstLogin.get(0);
			try {
				
				login.setStatus(Login.REFRESH);
				if(login.getExpireTime().before(new Date())) {
					login.setStatus(Login.EXPIRED);
				}
				
				loginMapper.insertArchive(login); // 1
				loginMapper.delete(login); // 2
				
				accessToken = super.refreshAccessToken(refreshTokenValue, tokenRequest);
				
				login.setId(Utils.getUUIDString());
				login.setAccessToken(accessToken.getValue());
				login.setRefreshToken(accessToken.getRefreshToken().getValue());
				login.setExpireTime(accessToken.getExpiration());
				login.setCreatedTime(new Date());
				login.setTokenObject(SerializationUtils.serialize((DefaultOAuth2AccessToken)accessToken));
				
				loginMapper.insert(login); // 3
			} catch (Exception e) {
				e.printStackTrace();
				throw new OAuth2Exception("internal server error");
			}
			
		}else {
			throw new OAuth2Exception("refresh token not found");
		}
		
		return accessToken;
	}

}