package id.co.hijr.oauth2.ref;

public enum ApplicationSiap {
	ID("300"),
	CODE("SIAP"),
	LINK("http://localhost:7076/siap");

	private final String text;

	private ApplicationSiap(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}
