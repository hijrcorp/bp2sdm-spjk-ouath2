package id.co.hijr.oauth2.ref;

public enum ApplicationSpjk {
	
	ID("200"),
	CODE("SPJK"),
	LINK("http://localhost:7077/spektra");

	private final String text;

	private ApplicationSpjk(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}
