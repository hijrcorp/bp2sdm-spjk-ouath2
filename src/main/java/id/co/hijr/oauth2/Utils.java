package id.co.hijr.oauth2;

import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

public class Utils {

	private static final long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;

	public static int randomPlus() {
		int min = 100;
		int max = 999;
		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}
	
	public static String getLongNumberID() {
		return (new Date().getTime() + "" + randomPlus());
	}
	
	public static String getUUIDString() {
		NoArgGenerator timeBasedGenerator = Generators.timeBasedGenerator();
		//Generate time based UUID
		UUID firstUUID = timeBasedGenerator.generate();
		return firstUUID.toString();
	}
	
	public static Date getUUIDDate(String uuidString) {
		return new Date((UUID.fromString(uuidString).timestamp() - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000);
	}
	
	public static long getDateDiffFromNow(Date d1) {
		Date d2 = new Date();
		long seconds = (d2.getTime()-d1.getTime())/1000;
		return seconds;
	}
	
	public static boolean ip(String text) {
		Pattern p = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
		Matcher m = p.matcher(text);
		return m.find();
	}
	
}
