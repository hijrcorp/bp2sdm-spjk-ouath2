package id.co.hijr.oauth2.model;

import org.springframework.http.HttpStatus;

public class ResponseWrapper {

	private int code = HttpStatus.OK.value();
	private String message = HttpStatus.OK.getReasonPhrase();
	private Object data;

	public ResponseWrapper() {
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
