package id.co.hijr.oauth2.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class Entity {
	
	private String id;
	private String locale = "en_US";
	
	private String personAdded;
	private Date timeAdded;
	private String personModified;
	private Date timeModified;
	
	public Entity() {
	}
	
	public Entity(String id) {
		this.id = id;
	}
	
	public Entity(String id, String locale) {
		this.id = id;
		this.locale = locale;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonIgnore
	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@JsonProperty("person_added")
	public String getPersonAdded() {
		return personAdded;
	}

	public void setPersonAdded(String personAdded) {
		this.personAdded = personAdded;
	}

	@JsonProperty("time_added")
	public Date getTimeAdded() {
		return timeAdded;
	}

	public void setTimeAdded(Date timeAdded) {
		this.timeAdded = timeAdded;
	}

	@JsonProperty("person_modified")
	public String getPersonModified() {
		return personModified;
	}

	public void setPersonModified(String personModified) {
		this.personModified = personModified;
	}

	@JsonProperty("time_modified")
	public Date getTimeModified() {
		return timeModified;
	}

	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

}
