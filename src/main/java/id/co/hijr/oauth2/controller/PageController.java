package id.co.hijr.oauth2.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.WebUtils;
import id.co.hijr.oauth2.mapper.AccountLoginMapper;
import id.co.hijr.oauth2.mapper.AccountMapper;
import id.co.hijr.oauth2.mapper.AuthoritiesMapper;
import id.co.hijr.oauth2.mapper.ConfigMapper;
import id.co.hijr.oauth2.mapper.ControlMapper;
import id.co.hijr.oauth2.mapper.Eselon1Mapper;
import id.co.hijr.oauth2.mapper.MappingJabatanUnitAuditiMapper;
import id.co.hijr.oauth2.mapper.MasterJabatanMapper;
import id.co.hijr.oauth2.mapper.SesiMapper;
import id.co.hijr.oauth2.mapper.UnitAuditiMapper;
import id.co.hijr.oauth2.model.Account;
import id.co.hijr.oauth2.model.AccountGroup;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.Application;
import id.co.hijr.oauth2.model.Authorities;
import id.co.hijr.oauth2.model.Control;
import id.co.hijr.oauth2.model.Eselon1;
import id.co.hijr.oauth2.model.MappingJabatanUnitAuditi;
import id.co.hijr.oauth2.model.MasterJabatan;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.ResponseWrapper;
import id.co.hijr.oauth2.model.UnitAuditi;

@Controller
public class PageController {
	
	@Value("${app.cookie.name}")
	private String cookieName;
	
	@Value("${app.cookie.used}")
	protected String cookieUsed;
	
	@Value("${user.picture.url}")
	private String pictureUrl;

	@Value("${app.state}")
	private String appState;
	
	@Value("${app.url.simpeg_key}")
	private String simpegKey;
	
	@Value("${source.default.id}")
	private String sourceDefaultId;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private AccountLoginMapper accountLoginMapper;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private AuthoritiesMapper authoritiesMapper;
	
	@Autowired
	private MappingJabatanUnitAuditiMapper mappingJabatanUnitAuditiMapper;
	
	@Autowired
	private Eselon1Mapper eselon1Mapper;
	
	@Autowired
	private UnitAuditiMapper unitAuditiMapper;
	
	@Autowired
	private MasterJabatanMapper jabatanMapper;
	
	@Autowired
	private ControlMapper controlMapper;
	
	@Autowired
	private SesiMapper sesiMapper;
	
	@Autowired
	private ConfigMapper configMapper;
	
	@Value("${main.application.id}") private String APP_SPJK_ID;
	@Value("${siap.application.id}") private String APP_SIAP_ID;

	@Value("${main.application.code}") private String APP_SPJK_CODE;
	@Value("${siap.application.code}") private String APP_SIAP_CODE;

	@Value("${main.application.name}") private String APP_SPJK_NAME;
	@Value("${siap.application.name}") private String APP_SIAP_NAME;

	@Value("${main.application.url}") private String APP_SPJK_LINK;
	@Value("${siap.application.url}") private String APP_SIAP_LINK;

	final public static String APP_SPJK_GROUP_ADMINISTRATOR = "1543544802020";
	final public static String APP_SPJK_GROUP_RESPONDEN = "2002200";
	final public static String APP_SPJK_GROUP_SATKER = "2002300";
	final public static String APP_SPJK_GROUP_ESELON1 = "2002400";
	final public static String APP_SPJK_GROUP_ADMIN_BDLHK = "2002600";
	final public static String APP_SPJK_GROUP_PUSDIKLAT = "2002700";
	final public static String APP_SPJK_GROUP_UNIT_PEMBINA = "2002800";
	final public static String APP_SPJK_GROUP_DEVELOPER = "9999999999999";

	final public static String APP_SIAP_GROUP_ADMINISTRATOR = "3002100";
	final public static String APP_SIAP_GROUP_ADMIN_SEKDIT = "3002200";
	final public static String APP_SIAP_GROUP_ADMIN_SATKER = "3002300";
	final public static String APP_SIAP_GROUP_AGEN_PERUBAHAN = "3002400";

	private Status status1 = new Status(1, "Silahkan isi Form Registrasi.");
	private Status status2 = new Status(2, "NIP anda sudah terdaftar pada aplikasi ${appBefore}. Silahkan mendaftar akun ${appDestination}.");
	private Status status3 = new Status(3, "NIP anda sudah terdaftar pada aplikasi ${appBefore}. Silahkan mengganti password pada halaman Login.");
	private Status status4 = new Status(4, "NIP anda tidak terdaftar pada aplikasi SIMPEG.");
	private Status status5 = new Status(5, "NIP anda sudah terdaftar pada aplikasi ${appDestination} sebagai ${appGroup} dan sedang menunggu persetujuan ADMIN.");
	private Status status6 = new Status(6, "NIP anda sudah terdaftar pada aplikasi ${appDestination} sebagai ${appGroup}. Silahkan Login.");
	private Status status9 = new Status(9, "Registrasi Berhasil.");
	
	@SuppressWarnings({ "unused", "resource" })
	public String getResourceFileAsString(String fileName) throws Exception {
		//System.out.println("haloo: " + ResourceUtils.getFile("classpath:"+fileName).getAbsolutePath());
		//File file = ResourceUtils.getFile("classpath:"+fileName);
		File file = new File("/Users/hadi/workspace/sts/bp2sdm-siap-ouath2/src/main/resources/public/json/"+fileName+".json");
		System.out.println("fileResource: " + file.getAbsolutePath());
		InputStream is = new FileInputStream(file);
		if (is != null) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			return reader.lines().collect(Collectors.joining(System.lineSeparator()));
		} else {
			return null;
		}
	}
	
	public List<Application> getListApplication() {
		List<Application> listApplication = new ArrayList<Application>();
		listApplication.add(new Application(APP_SPJK_ID, APP_SPJK_CODE, APP_SPJK_NAME, APP_SPJK_LINK));
		listApplication.add(new Application(APP_SIAP_ID, APP_SIAP_CODE, APP_SIAP_NAME, APP_SIAP_LINK));
		return listApplication;
	}
	
	public String getApplicationNameByCode(String APP_CODE) {
		String result = "";
		for(Application application : getListApplication()) {
			if(application.getCode().equalsIgnoreCase(APP_CODE)) { result = application.getName(); }
		}
		return result;
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/check-account", method = RequestMethod.GET)
	public ResponseEntity<ResponseWrapper> checkAccountApplication(@RequestParam String username, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		CheckAccount checkAccount = new CheckAccount();
		final List<Account> listAccount = accountMapper.getList(new QueryParameter(Account.USERNAME+"='"+username+"'"));
		if(listAccount.size() == 1) {
			checkAccount = new CheckAccount(listAccount.get(0));
			AccountGroup accountGroup = new AccountGroup();
			Application appBefore = new Application();
			boolean isAppSPJKExist = false;
			boolean isAppSIAPExist = false;
			System.out.println("checkAccount.getId(): "+checkAccount.getId());
			System.out.println("listAccount.get(0).getListAuthorities().size(): "+listAccount.get(0).getListAuthorities().size());
			for(Authorities authorities : listAccount.get(0).getListAuthorities()) {
				System.out.println("authorities.getApplicationId(): " + authorities.getApplicationId());
				if(authorities.getApplicationId().equals(APP_SPJK_ID)) { isAppSPJKExist = true; appBefore = authorities.getApplication(); accountGroup = authorities.getGroup(); }
				if(authorities.getApplicationId().equals(APP_SIAP_ID)) { isAppSIAPExist = true; appBefore = authorities.getApplication(); accountGroup = authorities.getGroup(); }
			}
			System.out.println("isAppSPJKExist: " + isAppSPJKExist);
			System.out.println("isAppSIAPExist: " + isAppSIAPExist);
			if(listAccount.get(0).isEnabled() && listAccount.get(0).isAccountNonExpired() && listAccount.get(0).isAccountNonLocked()) {
				if(isAppSIAPExist) {
					//System.out.println("listAccount.get(0).isEnabledAppSIAP(): " + listAccount.get(0).isEnabledAppSIAP());
					if(false) {//if(listAccount.get(0).isEnabledAppSIAP() == false) { // menunggu persetujuan ADMIN
						checkAccount.setStatus(status5.setMessage(status5.getMessage()
								.replace("${appDestination}", APP_SIAP_NAME)
								.replace("${appGroup}", accountGroup.getName())));
					}
					else { // silahkan login
						checkAccount.setStatus(status6.setMessage(status6.getMessage()
								.replace("${appDestination}", APP_SIAP_NAME)
								.replace("${appGroup}", accountGroup.getName())));
					}
				}
				// if (isAppSPJKExist) { 
				else { // sudah terdaftar di ${appBefore} silahkan mendaftar akun di ${appDestination}
					String appBeforeCode = getApplicationNameByCode(appBefore.getCode());
					String appDestinationCode = getApplicationNameByCode(getCookieTokenValue(request, "application"));
					if(appBeforeCode != null && appDestinationCode != null && appBeforeCode.equalsIgnoreCase(appDestinationCode)) {
						checkAccount.setStatus(status6.setMessage(status6.getMessage()
								.replace("${appDestination}", appDestinationCode)
								.replace("${appGroup}", accountGroup.getName())));
					} else {
						checkAccount.setStatus(status2.setMessage(status2.getMessage()
								.replace("${appBefore}", appBeforeCode)
								.replace("${appDestination}", appDestinationCode)));
					}
				}
			} else { // dikunci di semua aplikasi, harus ganti password dulu
				checkAccount.setStatus(status3.setMessage(status3.getMessage().replace("${appBefore}", appBefore.getCode())));
			}
		} else {
			// check status 6
			// JIKA NIP ADA DI SIMPEG
			if(org.apache.commons.lang3.StringUtils.isNumeric(username)) {
			
				String yourString = "";
				String nipPegawai = username;
				String url = simpegKey + "&nip="+nipPegawai;

				HttpStatus status = HttpStatus.OK;
				if(appState.equals("development")) {
					try {
						yourString = getResourceFileAsString(nipPegawai);
						System.out.println("LOCAL");;
					} catch (Exception e) {
						e.printStackTrace();
						resp.setCode(HttpStatus.BAD_REQUEST.value());
						resp.setMessage(status4.getMessage()); //[ON JSON LOCAL]
						checkAccount.setStatus(status4);
					}
				}else {
					try {
						RestTemplate restTemplate = new RestTemplate();
						ResponseEntity<String> resp2 = restTemplate.getForEntity(url, String.class);
						status = resp2.getStatusCode();
						yourString = resp2.getBody()!=null?resp2.getBody():"";
					} catch (HttpClientErrorException | ResourceAccessException | HttpServerErrorException f) {
						resp.setCode(HttpStatus.BAD_REQUEST.value());
						resp.setMessage(status4.getMessage()); // [ON SIMPEG]
						checkAccount.setStatus(status4);
					}
				}
				
				ObjectMapper mapper = new ObjectMapper();
				JsonNode root = null;
				if(status.equals(HttpStatus.OK)) {
					try {
						System.out.println("kodeSimpeg "+mapper.readTree(yourString));
						root = mapper.readTree(yourString);
						checkAccount = new CheckAccount();
						checkAccount.setId(null);
						checkAccount.setUsername(username);
						checkAccount.setName(root.path("nama").textValue());
						checkAccount.setMobile("");
						checkAccount.setStatus(status1);
						String kodeSimpeg = root.path("lok").textValue()+root.path("kdu1").textValue()+root.path("kdu2").textValue()+root.path("kdu3").textValue()+root.path("kdu4").textValue();
						List<UnitAuditi> listUnitAuditi = unitAuditiMapper.getList(new QueryParameter(UnitAuditi.KODE_SIMPEG_UNIT_AUDITI+"='"+kodeSimpeg+"'"));
						if(listUnitAuditi.size() > 0) {
							checkAccount.setUnitAuditi(listUnitAuditi.get(0));
							checkAccount.setEselon1(eselon1Mapper.getEntity(checkAccount.getUnitAuditi().getIdEselon1UnitAuditi()));
						}
						resp.setData(checkAccount);
					} catch (Exception e) {
						e.printStackTrace();
						checkAccount.setStatus(status4);
					}
				} else {
					checkAccount.setStatus(status4);
				}
			} else { // JIKA TIDAK ADA DI SIMPEG
				checkAccount.setStatus(status4);
			}
		}
		resp.setData(checkAccount);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	public void generateBasicAttribute(@RequestParam Optional<String> application, Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
		if(application.isPresent() && application.get().equals("") == false && application.get().equals("null") == false) {
			if(application.get().equalsIgnoreCase(APP_SPJK_CODE) || application.get().equalsIgnoreCase(APP_SIAP_CODE)) {
//				System.out.println("disini 1");
				model.addAttribute("application", application.get());
				model.addAttribute("APP_NAME", getApplicationNameByCode(application.get()));
				response.addCookie(new Cookie("application", application.get()));
			} else {
//				System.out.println("disini 2 application: " + application.get());
				response.addCookie(new Cookie("application", APP_SPJK_CODE));
				response.sendRedirect("login?application="+APP_SPJK_CODE);
			}
		}
		else {
			String applicationCookie = getCookieTokenValue(request, "application");
			System.out.println("applicationCookie: " + applicationCookie);
			if(applicationCookie == null || applicationCookie.equals("") || applicationCookie.equals("null")) {
				response.sendRedirect("login?application="+APP_SPJK_CODE.toLowerCase());
			} else {
				response.sendRedirect("login?application="+applicationCookie);
			}
		}
	}
	
	@RequestMapping("/login")
	public String login(@RequestParam Optional<String> application, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		generateBasicAttribute(application, model, request, response);
		return loadIndex(model, request, response);
	}
	
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public String register(@RequestParam Optional<String> application, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		generateBasicAttribute(application, model, request, response);
		return loadRegister(model, request, response);
	}
	
	@RequestMapping(value="/verify", method = RequestMethod.POST)
	public String verify(Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		String userId = request.getParameter("username");
		if(org.apache.commons.lang3.StringUtils.isNumeric(userId)) {
			if(configMapper.getEntityByKode("DISABLED_LOGIN")!=null) {
				if(configMapper.getEntityByKode("DISABLED_LOGIN").getStatus().equals("1")) {
					response.sendRedirect(request.getContextPath() + "/login?disabled");
				}
			}
		}
		
		if(userId != null) {
			System.out.println("Sedang mencoba login: "+userId);
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
			param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");
//			param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
//			param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");
//			param.setClause(param.getClause() + " OR " + AccountLogin.EMAIL + "='"+userId+"'");
//			param.setClause(param.getClause() + " OR " + AccountLogin.MOBILE + "='"+userId+"'");
			param.setClause(param.getClause() + ")");
			List<AccountLogin> lst = accountLoginMapper.getList(param);
			if(lst.size() > 0) {
				AccountLogin accountLogin = lst.get(0);
				boolean verify = accountLogin.isEnabled();
				
				model.addAttribute("verify", verify);
				if(verify) {
					model.addAttribute("action", request.getContextPath() + "/login");
				}else {
					model.addAttribute("action", request.getContextPath() + "/update-password");
				}
				model.addAttribute("realName", accountLogin.getFullName());
				model.addAttribute("cookieName", cookieName);
				model.addAttribute("userName", userId);
				model.addAttribute("email", accountLogin.getEmail());
				model.addAttribute("mobile", accountLogin.getMobile());
				model.addAttribute("picture", pictureUrl+accountLogin.getId());
				if(lst.size() > 1) {
					model.addAttribute("listSrc", lst);
				}else {
					model.addAttribute("source", accountLogin.getSource().getId());
				}
			}else {
				response.sendRedirect(request.getContextPath() + "/login?error");
			}
		}else {
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		return "index";
	}
	
	@RequestMapping(value="/password", method = RequestMethod.GET)
	public String password(Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			AccountLogin accountLogin = (AccountLogin)authentication.getPrincipal();
			model.addAttribute("verify", false);
			model.addAttribute("cookieName", cookieName);
			model.addAttribute("action", request.getContextPath() + "/update");
			model.addAttribute("realName", accountLogin.getFullName());
			model.addAttribute("userName", accountLogin.getUsername());
			model.addAttribute("email", accountLogin.getEmail());
			model.addAttribute("mobile", accountLogin.getMobile());
			model.addAttribute("picture", pictureUrl+accountLogin.getId());
			model.addAttribute("source", accountLogin.getSource().getId());
		} catch (Exception e) {
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		return "index";
	}
	
	@RequestMapping(value="/update-password", method = RequestMethod.POST)
	public String daftar(Model model, @RequestParam("email") Optional<String> email, @RequestParam("mobile") Optional<String> mobile, HttpServletRequest request, HttpServletResponse response)throws Exception {
		System.out.println("register");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if(username != null && password != null && !password.equals("")) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND (" + AccountLogin.USERNAME + "='"+username+"')");
			List<AccountLogin> lst = accountLoginMapper.getList(param);
			if(lst.size() > 0) {
				AccountLogin account = lst.get(0);
				account.setPassword(passwordEncoder.encode(password));
				if(email.isPresent()) account.setEmail(email.get());
				if(mobile.isPresent()) account.setMobile(mobile.get());
				account.setEnabled(true);
				accountMapper.update(account);
			}else {
				response.sendRedirect(request.getContextPath() + "/login?error");
			}
			response.sendRedirect(request.getContextPath() + "/login?register");
		}else {
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		return "index";
	}
	
	@RequestMapping(value="/update", method = RequestMethod.POST)
	public String update(
			@RequestParam("email") Optional<String> email, @RequestParam("mobile") Optional<String> mobile,
			Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		System.out.println("update");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		AccountLogin accountLogin = (AccountLogin)authentication.getPrincipal();
		System.out.println(accountLogin.getId());
		String password = request.getParameter("password");
		if(password != null && !password.equals("")) {
			AccountLogin account = accountLoginMapper.getEntity(accountLogin.getId()+accountLogin.getSource().getId());
			account.setPassword(passwordEncoder.encode(password));
			if(email.isPresent()) account.setEmail(email.get());
			if(mobile.isPresent()) account.setMobile(mobile.get());
			account.setEnabled(true);
			accountMapper.update(account);
			response.sendRedirect(request.getContextPath() + "/login?password");
		}else {
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		return "index";
	}
	
	//Urgent function, to fixed some user satker do mistake
	//Cause some issue when user satker create user
	//But the issue hasbeen solve, this function will be temporary
	@RequestMapping(value="/update-username", method = RequestMethod.GET)
	public String updateUsername(Model model, @RequestParam("username") Optional<String> username, HttpServletRequest request, HttpServletResponse response)throws Exception {
		System.out.println("update username");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		AccountLogin accountLogin = (AccountLogin)authentication.getPrincipal();
		System.out.println(accountLogin.getId());
		if(accountLogin != null) {
			Account account = accountMapper.getEntity(accountLogin.getId());
			account.setUsername(account.getUsername().replaceAll("\\s+",""));
			accountMapper.update(account);
			response.sendRedirect(APP_SPJK_LINK + "/login-logout?tryagain");
		}
		return "index";
	}
	
	@RequestMapping("/refresh")
	public void refresh(Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		Cookie cookie = new Cookie(cookieName, null);
		cookie.setMaxAge(-1);
		response.addCookie(cookie);
		response.sendRedirect(request.getContextPath());
	}
	
	@RequestMapping("/oauth/error")
	public String oauthError()throws Exception {
		return "error";
	}
	
	@RequestMapping("/")
	public String index(@RequestParam Optional<String> application, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		generateBasicAttribute(application, model, request, response);
		return loadIndex(model, request, response);
	}
	
	protected String extractHeaderToken(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaders("Authorization");
		while (headers.hasMoreElements()) { // typically there is only one (most servers enforce that)
			String value = headers.nextElement();
			if ((value.toLowerCase().startsWith(OAuth2AccessToken.BEARER_TYPE.toLowerCase()))) {
				String authHeaderValue = value.substring(OAuth2AccessToken.BEARER_TYPE.length()).trim();
				// Add this here for the auth details later. Would be better to change the signature of this method.
				request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE, value.substring(0, OAuth2AccessToken.BEARER_TYPE.length()).trim());
				int commaIndex = authHeaderValue.indexOf(',');
				if (commaIndex > 0) {
					authHeaderValue = authHeaderValue.substring(0, commaIndex);
				}
				return authHeaderValue;
			}
		}
		return "";
	}
	
	public static String getCookieTokenValue(HttpServletRequest request, String cookieName) {
		Cookie[] cookies = request.getCookies();
		String accessTokenValue = "";
		if(cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String name = cookies[i].getName();
				String value = cookies[i].getValue();
				if (name.equals(cookieName)) {
					accessTokenValue = value;
				}
			}
		}
		return accessTokenValue;
	}
	
	private String loadIndex(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			AccountLogin accountLogin = (AccountLogin)authentication.getPrincipal();
			model.addAttribute("picture", pictureUrl+accountLogin.getId());
			System.out.println("go - ga dulu");
			String applicationCookie = getCookieTokenValue(request, "application");
			if(applicationCookie.equalsIgnoreCase(APP_SPJK_CODE)) response.sendRedirect(APP_SPJK_LINK);
			if(applicationCookie.equalsIgnoreCase(APP_SIAP_CODE)) response.sendRedirect(APP_SIAP_LINK);
		} catch (Exception exception){
			//Invalid token
			//exception.printStackTrace();
			System.out.println("belum login");
			if(cookieUsed.equals("true")) {
				String accessTokenValue = WebUtils.getCookieTokenValue(request, cookieName);
				if (!accessTokenValue.equals("")) {
					try {
						DecodedJWT jwt = JWT.decode(accessTokenValue);
						model.addAttribute("verify", true);
						model.addAttribute("cookieName", cookieName);
						model.addAttribute("action", request.getContextPath() + "/login");
						model.addAttribute("userName", jwt.getClaim("account_id").asString());
						model.addAttribute("realName",jwt.getClaim("full_name").asString());
						model.addAttribute("organization", jwt.getClaim("organization_id").asString());
						model.addAttribute("picture", jwt.getClaim("account_avatar").asString());
					} catch (JWTDecodeException e){
						//Invalid token
						e.printStackTrace();
					}
				}
			}
			//model.addAttribute("application", application);
		}
		return "index";
	}
	

	private String loadRegister(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		model.addAttribute("action", request.getContextPath() + "/register");
		model.addAttribute("listEselon1", eselon1Mapper.getList(new QueryParameter()));
		model.addAttribute("listUnitAuditi", unitAuditiMapper.getList(new QueryParameter()));
		model.addAttribute("listJabatan", jabatanMapper.getList(new QueryParameter()));
		return "register";
	}

	@RequestMapping(value="/register", method = RequestMethod.POST)
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<ResponseWrapper> registerSubmit(Model model,
			@RequestParam("username") Optional<String> username,
			@RequestParam("full_name") Optional<String> fullName,
			@RequestParam("email") Optional<String> email,
			@RequestParam("mobile") Optional<String> mobile,
			@RequestParam("password") Optional<String> password,
			@RequestParam("idUnitAuditi") Optional<String> idUnitAuditi,
			@RequestParam("idJabatan") Optional<String> idJabatan,
			HttpServletRequest request,
			HttpServletResponse response)throws Exception {
//		if(username.isPresent()) System.out.println("username: " + username.get());
//		if(fullName.isPresent()) System.out.println("fullName: " + fullName.get());
//		if(email.isPresent()) System.out.println("email: " + email.get());
//		if(mobile.isPresent()) System.out.println("mobile: " + mobile.get());
//		if(idUnitAuditi.isPresent()) System.out.println("idUnitAuditi: " + idUnitAuditi.get());
//		if(idJabatan.isPresent()) System.out.println("idJabatan: " + idJabatan.get());

		ResponseWrapper resp = new ResponseWrapper();
		
		CheckAccount checkAccount = (CheckAccount) checkAccountApplication(username.get(), request, response).getBody().getData();
		
		System.out.println("checkAccount.getStatus().getId(): " + checkAccount.getStatus().getId() + " : " + checkAccount.getStatus().getMessage() );
		
		if(checkAccount.getStatus().getId() == 1 || checkAccount.getStatus().getId() == 2) {
			
			Account account = new Account(Utils.getLongNumberID());
			//if(username.isPresent()) account.setUsername(username.get()); sudah otomatis dari simpeg
			//if(fullName.isPresent()) account.setFirstName(fullName.get()); sudah otomatis dari simpeg
			account.setUsername(checkAccount.getUsername());
			account.setFirstName(checkAccount.getName());
			if(idUnitAuditi.isPresent() && checkAccount.getUnitAuditi().getIdUnitAuditi() == null) checkAccount.getUnitAuditi().setIdUnitAuditi(idUnitAuditi.get()); // kalau ada di checkAccount maka ambil dari form registrasi
			if(idJabatan.isPresent()) checkAccount.getJabatan().setId(idJabatan.get());
			if(email.isPresent()) account.setEmail(email.get());
			if(mobile.isPresent()) account.setMobile(mobile.get());
			if(password.isPresent()) account.setPassword(passwordEncoder.encode(password.get()));
			account.setLastName("");
			account.setEnabled(true);
			account.setAccountNonExpired(true);
			account.setCredentialsNonExpired(true);
			account.setAccountNonLocked(true);
			account.setTimeAdded(new Date());
			
			System.out.println("username: " + account.getUsername());
			System.out.println("fullName: " + account.getFirstName());
			System.out.println("email: " + account.getEmail());
			System.out.println("mobile: " + account.getMobile());
			System.out.println("idUnitAuditi: " + checkAccount.getUnitAuditi().getIdUnitAuditi());
			System.out.println("idJabatan: " + checkAccount.getJabatan().getId());

			boolean pass = true;
			if (account.getFirstName() == null || account.getLastName() == null && account.getUsername() == null || account.getPassword() == null || checkAccount.getUnitAuditi().getIdUnitAuditi() == null || checkAccount.getJabatan().getId() == null) {
				pass = false;
			} else {
				if (account.getFirstName().equals("") || /*account.getLastName().equals("") ||*/ account.getUsername().equals("") || account.getPassword().equals("") || checkAccount.getUnitAuditi().getIdUnitAuditi().equals("") || checkAccount.getJabatan().getId().equals("")) {
					pass = false;
				}
			}

			if (!pass) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}

			// insert 1. Silahkan isi Form Registrasi
			if(checkAccount.getStatus().getId() == 1) {
//				account.setEnabledAppSPJK(true);
//				account.setEnabledAppSIAP(false);
				accountMapper.insert(account);
			}
			// update 2. NIP anda sudah terdaftar pada aplikasi ${appBefore}, silahkan mendaftar akun Agen SIAP Berakhlak
			else if(checkAccount.getStatus().getId() == 2) {
				final List<Account> listAccount = accountMapper.getList(new QueryParameter(Account.USERNAME+"='"+username+"'"));
				if(listAccount.size() == 1) account = listAccount.get(0);
//				account.setEnabledAppSIAP(true);
				accountMapper.update(account);
			}

			String appGroup = APP_SPJK_GROUP_RESPONDEN;
			if(getCookieTokenValue(request, "application").equalsIgnoreCase(APP_SPJK_CODE)) appGroup = APP_SPJK_GROUP_RESPONDEN;
			if(getCookieTokenValue(request, "application").equalsIgnoreCase(APP_SIAP_CODE)) appGroup = APP_SIAP_GROUP_AGEN_PERUBAHAN;
			Authorities authorities = new Authorities(Utils.getLongNumberID());
			authorities.setAccountId(account.getId());
			authorities.setGroupId(appGroup);
			authorities.setApplicationId(getCookieTokenValue(request, "application"));
			authorities.setPersonAdded(account.getId());
			authorities.setTimeAdded(new Date());
			authoritiesMapper.insert(authorities);

			unitAuditiMapper.deleteAllUnitAuditiUser(account.getId());
			unitAuditiMapper.insertUnitAuditiUser(checkAccount.getUnitAuditi().getIdUnitAuditi(), account.getId());

			MappingJabatanUnitAuditi mappingJabatanUnitAuditi = new MappingJabatanUnitAuditi(Utils.getLongNumberID());
			mappingJabatanUnitAuditi.setIdJabatan(checkAccount.getJabatan().getId()); // TODO: input jabatan manual, mungkin nanti perlu otomatis
			mappingJabatanUnitAuditi.setIdAccount(account.getId());
			mappingJabatanUnitAuditi.setIdUnitAuditi(checkAccount.getUnitAuditi().getIdUnitAuditi()); // TODO: input unit auditi manual, tapi otomatis kalau ada didatabase
			mappingJabatanUnitAuditi.setIdSesi(sesiMapper.getEntityActive().getId());
			mappingJabatanUnitAuditi.setGolongan(null);
			mappingJabatanUnitAuditiMapper.insert(mappingJabatanUnitAuditi);
			
			Control control = new Control(Utils.getLongNumberID());
			control.setAccountId(account.getId());
			control.setSourceId(sourceDefaultId);
			
			if(authorities.getGroupId().equals(APP_SPJK_GROUP_RESPONDEN) || authorities.getGroupId().equals(APP_SIAP_GROUP_AGEN_PERUBAHAN)) {
				control.setIsAdmin(false);
			}else {
				control.setIsAdmin(true);
			}
			controlMapper.insert(control);
			
			checkAccount.setStatus(status9);
		}
		
		resp.setData(checkAccount);
		
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
}

class CheckAccount {
	private String id;
	private String username;
	private String name;
	private String email;
	private String mobile;
	private Eselon1 eselon1 = new Eselon1();
	private UnitAuditi unitAuditi = new UnitAuditi();
	private MasterJabatan jabatan = new MasterJabatan();
	private Status status;
	public CheckAccount(Account account) {
		this.id = account.getId();
		this.username = account.getUsername();
		this.name = account.getName();
		this.email = account.getEmail();
		this.mobile = account.getMobile();
	}
	public CheckAccount() { }
	public String getId() { return id; }
	public String getUsername() { return username; }
	public String getName() { return name; }
	public String getEmail() { return email; }
	public String getMobile() { return mobile; }
	public Eselon1 getEselon1() { return eselon1; }
	public UnitAuditi getUnitAuditi() { return unitAuditi; }
	public MasterJabatan getJabatan() { return jabatan; }
	public Status getStatus() { return status; }
	public void setId(String id) { this.id = id; }
	public void setUsername(String username) { this.username = username; }
	public void setName(String name) { this.name = name; }
	public void setEmail(String email) { this.email = email; }
	public void setMobile(String mobile) { this.mobile = mobile; }
	public void setEselon1(Eselon1 eselon1) { this.eselon1 = eselon1; }
	public void setUnitAuditi(UnitAuditi unitAuditi) { this.unitAuditi = unitAuditi; }
	public void setJabatan(MasterJabatan jabatan) { this.jabatan = jabatan; }
	public void setStatus(Status status) { this.status = status; }
}

class Status {
	private int id;
	private String message;
	public Status(int id, String message) {
		this.id = id;
		this.setMessage(message);
	}
	public int getId() { return id; }
	public String getMessage() { return message; }
	public Status setMessage(String message) { this.message = message; return this; }
}
