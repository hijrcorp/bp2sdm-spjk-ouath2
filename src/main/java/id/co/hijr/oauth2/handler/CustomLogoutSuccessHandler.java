package id.co.hijr.oauth2.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		if(request.getParameter("redirect_uri") == null) {
			System.out.println("BANGSATTTT");
			response.sendRedirect(request.getContextPath() + "/login?logout&application="+request.getParameter("application"));
		}else {
			response.sendRedirect(request.getParameter("redirect_uri")+"?application="+request.getParameter("application"));
		}
	}

}
