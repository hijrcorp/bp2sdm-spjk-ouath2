<%@page import="java.util.List" %>
<%@page import="java.util.Arrays" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Enumeration" %>
<%@page import="java.util.Collections" %>
<%
	out.println("<table border='1' width='100%' style='color:white;'>");
	List<String> listAttName = new ArrayList<String>();
	for(Enumeration<String> e = request.getAttributeNames(); e.hasMoreElements(); ){
		String attName = (String)e.nextElement();
		if(!attName.contains("org.") && !attName.contains("javax") && !attName.contains("spring") && !attName.contains("FILTERED") && !attName.contains("browser") &&
				!attName.contains("locale") && !attName.contains("ssoPath") && !attName.contains("OAuth2") && !attName.contains("_csrf")) listAttName.add(attName);
	}
	Collections.sort(listAttName);
	for(String attName : listAttName) {
		out.println("<tr>");
		out.println("<td>"+attName+"</td>");
		out.println("<td>");
		if(!attName.equals("roleList")) out.println(request.getAttribute(attName));
		else {
			out.println("<ol style='margin-left:35px;'>");
			String roleList[] = request.getAttribute(attName).toString().replace("[", "").replace("]", "").split(", ");
			Arrays.sort(roleList);
			for(String role : roleList) { out.println("<li>"+role+"</li>"); }
			out.println("</ol>");
		}
		out.println("</td>");
		out.println("</tr>");
	}
	out.println("</table>");
%>
</body>