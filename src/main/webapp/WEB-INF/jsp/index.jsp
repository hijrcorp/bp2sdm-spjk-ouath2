<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
	<title>Single Sign On (SSO)</title>
	<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css">
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
</head>
<body>
	<div class="top-content">
		<div class="inner-bg">
			<div class="container">
				<%-- <%@include file="attribute.jsp" %> --%>
				<div class="row">
					<div class="col-lg-4 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-6 col-sm-offset-3 form-box">
						<div class="form-top">
							<div class="form-top-left">
								<h5 id="app-name" style="color:white">${APP_NAME}</h5>
								<h4>Single Sign On (SSO) </h4>
								<p><spring:message code="test.label"/></p>
								<c:if test="${param.error != null}">
									<p style="font-size: 20; color: #FF1C19;">Data yang anda masukkan salah, silahkan coba kembali.	</p>
								</c:if>
								<c:if test="${param.duplicate != null}">
									<p style="font-size: 20; color: #FF1C19;">Sesi Login anda telah habis/Anda telah melakukan login pada device yang lain.</p>
								</c:if>
								<c:if test="${param.sesierror != null}">
									<p style="font-size: 20; color: #FF1C19;"><spring:message code="test.sesierror"/></p>
								</c:if>
								<c:if test="${param.connection != null}">
									<p style="font-size: 20; color: #FF1C19;">Mohon maaf, sepertinya terjadi kesalahan pada server atau pastikan koneksi internet anda tidak bermasalah, mohon coba beberapa saat lagi..</p>
								</c:if>
								<c:if test="${param.banned != null}">
									<p style="font-size: 20; color: #FF1C19;">Sistem mendeteksi anda, mencoba membuka mode developer, jika anda tidak mengetahui hal ini, mohon laporkan ke Admin. <br/>*informasi ini akan dicatat ke sistem</p>
								</c:if>
								<c:if test="${param.maintenance != null}">
									<p style="font-size: 20; color: #FF1C19;">Mohon maaf, server sedang dalam maintenance</p>
								</c:if>
								<c:if test="${param.register != null}">
									<p style="font-size: 20; color: green;">Pendaftaran telah berhasil, silahkan melakukan login kembali.</p>
								</c:if>
								<c:if test="${param.password != null}">
									<p style="font-size: 20; color: green;">Password anda telah berhasil diubah.</p>
								</c:if>
								<c:if test="${param.disabled != null}">
									<p style="font-size: 20; color: #FF1C19;">Login belum diaktifkan, mohon hubungi admin.	</p>
								</c:if>
								<c:if test="${param.tryagain != null}">
									<p style="font-size: 20; color: #FF1C19;">Sistem mendeteksi nip anda menggunakan tanda spasi, Silahkan Login lagi, tanpa menggunakan tanda spasi.</p>
								</c:if>
							</div>
							<div class="form-top-right">
								<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
							</div>
						</div>
						<c:choose>
							<c:when test="${(param.success != null || pageContext.request.userPrincipal != null) && verify == null}">
								<div class="form-bottom">
									<form class="login-form">
										<p>
											<img class="img-circle" src="${picture}" width="28px" /> <span style="font-family:arial; color:White; font-size: 18px;"><sec:authentication property="principal.fullName" /></span>
											<span style="font-family:arial; font-size: 14px;"><a href="${pageContext.request.contextPath}/password">(update)</a></span>
										</p>
										<p> Anda mengelola sumber: 
											<br><span style="font-family:arial; color:White; font-size: 18px;font-weight: bold;"><sec:authentication property="principal.source.name" /></span>
										</p>
										<button id="btnLogout" type="button" class="btn" onclick="location.href='logout'">Keluar</button>
									</form>
								</div>
							</c:when>
							<c:otherwise>
								<div class="form-bottom">
									<c:choose>
										<c:when test="${verify != null && verify == true}">
										<form role="form" action="${action}" method="post" class="login-form" name="f">
											<div class="form-group">
												<p><img class="img-circle" src="${picture}" width="28px" /> <span style="font-family:arial; color:White; font-size: 18px;">${realName} </span>
												<input type="hidden" name="username" id="username" value="${userName}"> <a href="${pageContext.request.contextPath}/refresh">(change)</a></span>
											</div>
											<c:choose>
												<c:when test="${listSrc != null}">
													<div class="form-group">
														<label class="sr-only" for="form-source">Sumber</label>
														<select name="source" id="source" class="form-source form-control">
																<option value="">Pilih Sumber</option>
																<c:forEach items="${listSrc}" var="item">
																<option value="${item.source.id}">${item.source.name}</option>
															</c:forEach>
														</select>
													</div>
												</c:when>
												<c:otherwise>
													<input type="hidden" name="source" id="source" value="${source}">
												</c:otherwise>
											</c:choose>
											<!-- <div class="form-group">
												<label class="sr-only" for="form-password">Password</label>
												<input type="password" name="password" placeholder="Password" class="form-password form-control" id="password">
											</div> -->
											<div class="form-group">
												<label class="sr-only" for="form-password">Password</label>
												<div class="input-group" id="show_hide_password">
													<input class="form-password form-control" type="password" name="password" placeholder="Password" id="password" value="4p4r4tur">
													<div class="input-group-addon">
														<a href="javascript:void(0)" id="show_password"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="checkbox">
													<label><input type="checkbox" name="remember-me"> <span style="color:White;">Tetap masuk</span></label>
												</div>
											</div>
										</c:when>
										<c:when test="${verify != null && verify == false}">
											<form role="form" action="${action}" method="post" class="login-form" name="f">
												<div class="form-group">
													<p><img class="img-circle" src="${picture}" width="28px" /> <span style="font-family:arial; color:White; font-size: 18px;">${realName} </span>
													<input type="hidden" name="username" id="username" value="${userName}"> <a href="${pageContext.request.contextPath}">(back)</a></span>
												</div>
												<!-- <div class="form-group">
													<label class="sr-only" for="form-password">Password</label>
													<input type="password" name="password" placeholder="Password Baru" class="form-password form-control" id="password">
												</div> -->
												<div class="form-group">
													<label class="sr-only" for="form-password">Password</label>
													<div class="input-group" id="show_hide_password">
														<input class="form-password form-control" type="password" name="password" placeholder="Password Baru" id="password" value="4p4r4tur">
														<div class="input-group-addon">
															<a href="javascript:void(0)" id="show_password"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
														</div>
													</div>
													<small class="text-success">Tips:Gunakan password yang mudah diingat.</small>
												</div>
												<div class="form-group">
													<label class="sr-only" for="form-email">Email</label>
													<input type="text" name="email" placeholder="Email (Wajib)" class="form-control" id="email" value="${email}" required>
												</div>
												<div class="form-group">
													<label class="sr-only" for="form-mobile">HP</label>
													<input type="text" name="mobile" placeholder="HP (Wajib) ex: 0811234567" class="form-control" id="mobile" value="${mobile}" required>
													<small class="text-success d-none hidden">Tips: Sebaiknya dilengkapi, agar admin dapat menghubungi anda, apabila batas waktu ujian akan berakhir.</small>
												</div>
												<%-- <div class="form-group">
													<label class="sr-only" for="form-mobile">Golongan</label>
													<select name="id_golongan" class="form-control theselect" id="id_golongan">
														<option>Pilih Golongan</option>
														value="${mobile}"
													</select>
												</div> --%>
											</c:when>
											<c:otherwise>
											<form role="form" action="${pageContext.request.contextPath}/verify" method="post" class="login-form" name="f">
												<div class="form-group">
													<label class="sr-only" for="form-username">Username</label>
													<input autocomplete="off" type="text" name="username" placeholder="NIP/Username" class="form-username form-control" id="username" value="admin">
												</div>
											</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${param.logout != null}">
												<button id="btnLogin" type="submit" class="btn">Masuk Kembali</button>
											</c:when>
											<c:when test="${verify != null && verify == false}">
												<button id="btnLogin" type="submit" class="btn">Update</button>
											</c:when>
											<c:otherwise>
												<button id="btnLogin" type="submit" class="btn">Masuk</button>
											</c:otherwise>
										</c:choose>
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
									</form>
									<c:choose>
										<c:when test="${verify != null && verify == true}"></c:when>
										<c:when test="${verify != null && verify == false}"></c:when>
										<c:otherwise>
											<div class="form-group"><a title='klik untuk daftar akun baru' href="register?application=${application}">Belum punya akun.</a></div>
										</c:otherwise>
									</c:choose>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
<script src="${pageContext.request.contextPath}/js/login-script.js"></script>
</html>