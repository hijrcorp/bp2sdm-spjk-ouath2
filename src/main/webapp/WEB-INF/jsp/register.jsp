<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
	<title>Form Registration - ${APP_NAME}</title>
	<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css">
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
</head>
<body>
	<div class="top-content">
		<div class="inner-bg">
			<div class="container">
				<%-- <%@include file="attribute.jsp" %> --%>
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-6 col-sm-offset-3 form-box">
						<div class="form-top">
							<div class="form-top-left">
								<h3 id="app-name">${APP_NAME}</h3>
								<h4>Form Registration</h4>
								<%-- <p><spring:message code="test.register"/></p> --%>
								<c:if test="${param.error != null}">
									<p style="font-size: 20; color: #FF1C19;">Data yang anda masukkan salah, silahkan coba kembali.	</p>
								</c:if>
								<c:if test="${param.duplicate != null}">
									<p style="font-size: 20; color: #FF1C19;">Sesi Login anda telah habis/Anda telah melakukan login pada device yang lain.</p>
								</c:if>
								<c:if test="${param.sesierror != null}">
									<p style="font-size: 20; color: #FF1C19;"><p><spring:message code="test.sesierror"/></p></p>
								</c:if>
								<c:if test="${param.connection != null}">
									<p style="font-size: 20; color: #FF1C19;">Mohon maaf, sepertinya terjadi kesalahan pada server atau pastikan koneksi internet anda tidak bermasalah, mohon coba beberapa saat lagi..</p>
								</c:if>
								<c:if test="${param.banned != null}">
									<p style="font-size: 20; color: #FF1C19;">Sistem mendeteksi anda, mencoba membuka mode developer, jika anda tidak mengetahui hal ini, mohon laporkan ke Admin. <br/>*informasi ini akan dicatat ke sistem</p>
								</c:if>
								<c:if test="${param.maintenance != null}">
									<p style="font-size: 20; color: #FF1C19;">Mohon maaf, server sedang dalam maintenance</p>
								</c:if>
								<c:if test="${param.email != null}">
									<p style="font-size: 20; color: green;">Pendaftaran telah berhasil, silahkan menunggu persetujuan ADMIN dan Login.</p>
									<!-- <script>alert('Pendaftaran telah berhasil, silahkan menunggu persetujuan ADMIN dan Login.'); location='../login';</script> -->
								</c:if>
								<%-- <c:if test="${param.password != null}">
									<p style="font-size: 20; color: green;">Password anda telah berhasil diubah.</p>
								</c:if> --%>
							</div>
							<div class="form-top-right">
								<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
							</div>
						</div>
						<div class="form-bottom">
							<form role="form" action="${action}" method="post" class="register-form" name="f">
								<!-- <div class="alert alert-danger" role="alert">
									This is a danger alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
								</div> -->
								<div id="msg-alert"></div>
								<div class="row">
									<div class="form-group col-md-6">
										<label class="sr-only" for="form-username">Username</label>
										<div class="input-group"><!-- 197506271999031001 -->
											<input autocomplete="off" type="text" name="username" placeholder="NIP" class="form-username form-control" id="username" required maxlength="18" value="" onchange="disabledForm()" onkeyup="disabledForm()">
											<span class="input-group-btn">
											<button class="btn btn-outline-secondary" type="button" id="btnCheck" onclick="btnCheckNip()">Check</button>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-12">
										<label class="sr-only" for="form-username">Nama Lengkap</label>
										<input autocomplete="off" type="text" name="full_name" placeholder="Nama Lengkap" class="form-username form-control" id="full_name" required disabled>
									</div>
								</div>
								<div class="row">
									<%-- <div class="form-group col-md-6">
										<label class="sr-only" for="form-eselon1">Eselon1</label>
										<!-- <input type="text" name="idEselon1" placeholder="Eselon1" class="form-control" id="idEselon1" value="" required disabled> -->
										<select id="idEselon1" name="idEselon1" class="form-control" readonly disabled>
											<option value="">-- Pilih Eselon1 --</option>
											<c:forEach items="${listEselon1}" var="o" varStatus="loopStatus">
												<option value="${o.id}">${o.nama}</option>
											</c:forEach>
										</select>
									</div> --%>
									<div class="form-group col-md-6">
										<label class="sr-only" for="form-unitAuditi">Unit Auditi</label>
										<!-- <input type="text" name="idUnitAuditi" placeholder="Unit Auditi" class="form-control" id="idUnitAuditi" value="" required disabled> -->
										<select id="idUnitAuditi" name="idUnitAuditi" class="form-control" required disabled >
											<option value="">-- Pilih Unit Auditi --</option>
											<c:forEach items="${listUnitAuditi}" var="o" varStatus="loopStatus">
												<option value="${o.idUnitAuditi}">${o.namaUnitAuditi}</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label class="sr-only" for="form-unitAuditi">Jabatan</label>
										<!-- <input type="text" name="idJabatan" placeholder="Jabatan" class="form-control" id="idJabatan" value="" required disabled> -->
										<select id="idJabatan" name="idJabatan" class="form-control" required disabled >
											<option value="">-- Pilih Jabatan --</option>
											<c:forEach items="${listJabatan}" var="o" varStatus="loopStatus">
												<option value="${o.id}">${o.nama}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label class="sr-only" for="form-email">Email</label>
										<input type="email" name="email" placeholder="Email" class="form-control" id="email" value="" required disabled>
									</div>
									<div class="form-group col-md-6">
										<label class="sr-only" for="form-mobile">HP</label>
										<input type="text" name="mobile" placeholder="HP ex: 0811234567" class="form-control" id="mobile" value="" required disabled>
										<small class="text-success d-none hidden">Tips: Sebaiknya dilengkapi, agar admin dapat menghubungi anda, apabila batas waktu ujian akan berakhir.</small>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label class="sr-only" for="form-password">Password</label>
										<div class="input-group" id="show_hide_password">
											<input class="form-password form-control" type="password" name="password" placeholder="Password" id="password" required disabled>
											<div class="input-group-addon">
												<a href="javascript:void(0)" id="show_password"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
											</div>
										</div>
									</div>
									<div class="form-group col-md-6">
										<label class="sr-only" for="form-password">Password</label>
										<div class="input-group" id="show_hide_password">
											<input class="form-password form-control" type="password" name="password2" placeholder="Konfirmasi Password" id="password2" required disabled>
											<div class="input-group-addon">
												<a href="javascript:void(0)" id="show_password2"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
											</div>
										</div>
										<%-- <small class="text-success">Tips:Gunakan password yang mudah diingat.</small> --%>
									</div>
								</div>
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
								<button id="btnLogin" type="submit" class="btn" disabled>Register</button>
							</form>
							<div class="form-group"><a title='klik untuk login.' href="${pageContext.request.contextPath}/" class="pull-right">Sudah punya akun.</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
<script src="${pageContext.request.contextPath}/js/register-script.js"></script>
</html>