-- 1. 
ALTER TABLE `hijr_application` CHANGE `name_application` `name_application` VARCHAR(150)  CHARACTER SET latin1 COLLATE latin1_swedish_ci  NOT NULL DEFAULT '';

-- 2. 
ALTER TABLE `hijr_authorities` ADD `enabled_authorities` TINYINT(1)  NOT NULL  DEFAULT 1 AFTER `application_id_authorities`;

-- 3. 
INSERT INTO `hijr_application` (`id_application`, `code_application`, `name_application`, `prefix_role_application`, `organization_id_application`, `link_application`, `default_role_application`, `person_added_application`, `time_added_application`, `person_modified_application`, `time_modified_application`)
VALUES ('300', 'SIAP', 'Sistem Pemetaan Jabatan Agen Perubahan Pelayanan Akuntabel Kompeten Harmonis Loyal Adaptif Kolaboratif', 'ROLE_SIAP_', '', '', 'ROLE_SIAP_USER', NULL, NULL, NULL, NULL);

-- 4. 
INSERT INTO `hijr_account_group` (`id_account_group`, `name_account_group`, `description_account_group`, `application_id_account_group`, `sequence_account_group`)
VALUES
	('3002100', 'Administrator', 'Full Akses', '300', 1),
	('3002200', 'Admin Sekdit', 'Maksudnya Admin Eselon1', '300', 2),
	('3002300', 'Admin Satker', 'Yang nyuruh agen untuk bikin pernyataan', '300', 3),
	('3002400', 'Agen Perubahan', 'Yang ngerjain bikin pernyataan', '300', 4);

-- 5.
INSERT INTO `oauth_client_details` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`)
VALUES
	('siap-web', 'hijr_core', 'c5fa05675126f5a343a69a1657ff0e15', 'read,write', 'authorization_code,password,refresh_token', '', 'ROLE_USER', 31536000, 30000, '{}', 'read,write');



-- other
CREATE VIEW oauth_account_application AS
SELECT
a.*,
SUM(IF(application_id_authorities = 200, 1, 0)) AS SPJK,
SUM(IF(application_id_authorities = 300, 1, 0)) AS SIAP
FROM oauth_account_login a
LEFT JOIN hijr_authorities b ON a.id_account = b.account_id_authorities
GROUP BY username_account